# Textual Data Analysis Tutorial

Qixuan Yang (Update: 6th of April, 2019)

PLSC 799 Microhistorical Analysis in Social Science Research, Prof. Isabela Mares

Department of Political Science, Yale University

## Introduction

Please download this repository by clicking the cloud symbol next to "Find file" above as a .zip file. Please unzip the folder on your local computer.

The repository features the following files:

- ``syllabus.pdf`` [Syllabus (Click to view)](syllabus.pdf)
- ``tutorial_1.R`` R-script for the first session (April 9th)
- ``tutorial_2.R`` R-script for the first session (April 16th)
- ``obama_sample1`` Directory of sample Obama Archive documents, V1
- ``obama_sample2`` Directory of sample Obama Archive documents, V2
- ``obama_sample_1.csv`` CSV of sample Obama Archive documents, V1
- ``obama_sample_2.csv`` CSV of sample Obama Archive documents, V2
- ``obama_wiki_ch.txt`` Wikipedia info of Obama in Chinese (wrong encoding)
- ``obama_wiki_german.txt`` Wikipedia info of Obama in German (wrong encoding)
- ``data_archive.zip`` Replication files for data collection

Prerequisite:
1. Please have R (https://cloud.r-project.org) and R-Studio (https://www.rstudio.com/products/rstudio/download/, Free version) installed. 
2. Please have the following packages installed (use ``install.packages()``)
    - ``tidyverse``
    - ``tidytext``
    - ``tm``
    - ``tokenizers``
    - ``text2vec``
    - ``quanteda``
    - ``topicmodels``

## Notes
This tutorial uses the Obama archive (https://obamawhitehouse.archives.gov) as the example for analysis. Regarding the copyrights for these document in academic research and other non-commercial usage, please refer to the instruction of its homepage.

## License
<a href="http://www.wtfpl.net/"><img
       src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png"
       width="88" height="31" alt="WTFPL" /></a>