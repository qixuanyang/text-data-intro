
	University of North Florida
	Jacksonville, Florida


	3:29 P.M. EDT


	THE PRESIDENT:  Thank you!  (Applause.)  Thank you!  (Applause.)  Thank you, guys.  Thank you.  (Applause.)  Thank you.  So I’m so sorry that we can’t fit everybody in, but I just wanted to come by and say thank you.  Thank you.  (Applause.)  


	I’m proud of all of you.  I’m so grateful for everything you’ve done to support me.  Except we’ve got a couple guys with the Trump -- you’re at the wrong rally.  (Laughter.)  


	AUDIENCE:  Booo --


	THE PRESIDENT:  No, no, no, it’s good.  But I just want you -- I just want everybody out there to know that I couldn’t have done what we’ve done without you.  (Applause.)  And Hillary Clinton won’t be able to do what she needs to do without you, so I need everybody to go out there and vote.  (Applause.)  We’ve only got five days left, so don’t hold anything back. 


	And I know if you’re at this rally, you probably voted.  (Applause.)  But I want to -- talk to your cousins, your neighbors, your friends, your uncles, folks at the barbershop, at the beauty shop.  Make sure every single person out there makes their voice heard in this critical election.  


	I love you guys.  Let’s get to work.  Thank you.  (Applause.)  


	END                 
	3:41 P.M. EDT

