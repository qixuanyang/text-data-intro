
	Hyatt Regency
	Atlanta, Georgia


	1:39 P.M. EDT


	THE PRESIDENT:  Thank you, DAV!  (Applause.)  Thank you so much.  Thank you.  Please, everybody, have a seat.  What an honor to be with you today.  (Applause.)  Thank you.  And thanks to Bobby.  I’ll never forget the time Bobby came to the Oval Office -- carrying a baseball bat.  (Laughter.)  Secret Service got a little nervous.  (Laughter.)  But it was a genuine Louisville Slugger -- a thank-you for going to bat for our veterans.  And I want to thank Bobby for your devotion to our veterans, especially your fellow Vietnam vets.  Thank you.  Give Bobby a big round of applause.  (Applause.)


	AUDIENCE MEMBER:  I love you!


	THE PRESIDENT:  I love you back.  (Applause.)  I do!    


	I want to thank our outstanding leadership team for welcoming me today, including National Commander Moses McIntosh. (Applause.)  Senior Vice Commander Dave Riley.  (Applause.)    National Adjutant Mark Burgess.  (Applause.)  Executive Director Barry Jesinoski.  (Applause.)  Your voice in Washington, Garry Augustine.  (Applause.)  And, don't forget, Pat Kemper and all the spouses and families of the DAV Auxiliary.  Thank you.  (Applause.) 


	I also want to acknowledge Mayor Kasim Reed and County Chairman John Eaves for welcoming us to the great state of Georgia and the beautiful city of Atlanta.  (Applause.)  I am pleased to be joined by our tireless Secretary of the VA, Bob McDonald.  (Applause.)  I know he spoke to you yesterday.  He is working hard -- hard -- every single day to transform the VA to serve our veterans better.  He still gives out his cellphone number and his email.  Not many people know this, but, so far, he’s received more than 45,000 calls, emails and texts.  (Laughter.)  And I don't know what his phone bill is looking like -- (laughter) -- I hope he has a good plan.  (Laughter.)  But Bob and his team work to deal with each one of those texts or emails or phone calls he receives because every single veteran matters. And he knows that.  So, thank you, Bob, for the great work you're doing.  (Applause.)  
	    
	So it’s good to be back with the Disabled American Veterans. What a journey that we’ve had together.


	AUDIENCE MEMBER:  Glad to have you!


	THE PRESIDENT:  It's great to be here.  


	We worked together back when I was a senator.  You were one of the first veterans’ organizations I called when I ran for President.  I welcomed you to the White House as a partner.  I came to your convention in my first term, and my second, along with Michelle.  And so it is fitting that my final major address to our nation’s veterans as President is here at the DAV.  (Applause.)  


	And as I reflect on these past eight years, some of the most unforgettable experiences that I’ve had have been moments I’ve spent with you -- America’s veterans and your families.  We stood together at Arlington to honor Corporal Frank Buckles, 110 years old -- our last veteran from the First World War -- as he was laid to rest.  I ordered our flags to be flown at half-staff because, even after 100 years, we will never stop saluting those who served in our name.  (Applause.)  


	We stood together at Normandy to thank an entire generation -- among them, my grandfather, who was in Patton’s Army -- a generation that literally saved the world.  There was Harry Kulkowitz, who returned to the beaches he helped liberate -- and told he could have anything he wanted, said with the humility of a soldier, a hamburger will do just fine.  (Laughter.)  I think of Luta McGrath -- this past Veterans Day, just before her 108th birthday, then the oldest known female veteran of World War II, which was a reminder that women have always served to keep America strong and free.  (Applause.)  


	We’ve stood together at the memorial to our Korean War veterans and recalled how a soldier, marching through the snow, had a tiny pair of baby booties hanging from his rifle -- a reminder of his unborn child -- a story that had been lost to history.  But we tracked him down.  We found him.  And we shared the story of Korean War veteran Dick Shank, who made it home to that baby boy, and lived out his life -- at 84 years old, he was still roller skating -- because no war should ever be forgotten and no veteran should ever be overlooked.  (Applause.)  


	We’ve stood together at the Wall and remembered the lessons of Vietnam -- that even when Americans may disagree about a war, we have to stand united in support of our troops.  (Applause.)  And that for mothers like Sarah Shay, who honored her missing son for more than 40 years, we will never stop working to bring home our prisoners of war and our missing in action.  We leave nobody behind.  No one.  (Applause.)  
	  
	And we’ve come together to welcome our newest veterans into your ranks -- from Desert Storm, the Balkans, Afghanistan, and Iraq -- our proud 9/11 Generation.  This is a time of transition. When I came into office, we had nearly 180,000 American troops in Afghanistan and Iraq.  Today, that number is less than 15,000.  Most of our troops have come home.  (Applause.)  


	To all of you who served in Afghanistan, you can take enormous pride in the progress you helped achieve:  Driving al Qaeda out of its camps.  Toppling the Taliban.  Delivering justice to Osama bin Laden.  Helping Afghans improve their lives. There are millions of boys and girls in school, and democratic elections and a democratic government.  Training Afghan forces to take responsibility for their own security so that we are no longer engaged in a major ground war in Afghanistan.  That is your legacy.  And today we salute our forces serving there on a more limited mission -- supporting Afghan forces, going after terrorists -- because we must never allow Afghanistan to be used as a safe haven for terrorists to attack our nation again.  (Applause.)  


	To all of you who served in Iraq, we saw your heroism in pushing out a dictator whose brutality must be condemned, never praised.  In defeating an insurgency.  In giving the Iraqi people a chance.  And no matter what has happened since, your valor in the deserts, in fierce urban combat, will be honored in the annals of military history. 


	Let me say something else about this generation.  As Commander-in-Chief, I’m pretty tired of some folks trash-talking America’s military and troops.  (Applause.)  Our military is somewhat smaller -- after two major ground wars come to a close, that’s natural.  And we’re going to keep doing everything we need to do to improve readiness and modernize our forces.  But let’s get some facts straight.  America’s Army is the best-trained, best-equipped land force on the planet.  (Applause.)  Our Navy is the largest and most lethal in the world.  (Applause.)  The precision of, and reach of, our Air Force is unmatched.  (Applause.)  Our Marines are the world’s only truly expeditionary force.  (Applause.)  We have the world’s finest Coast Guard.  (Applause.)  We have the most capable fighting force in history  -- and we’re going to keep it that way.  (Applause.)  


	And no ally or adversary should ever doubt our strength and our resolve.  And we will keep pounding ISIL and taking out their leaders, and pushing them back on the ground.  And united with a global coalition, we will destroy this barbaric terrorist group. They will be destroyed.  (Applause.)  


	In the face of Russian aggression, we’re not going to turn our back to our allies in Europe.  We’re going to stay united in NATO, which is the world’s strongest alliance.  (Applause.)  From the Asia Pacific to Africa to the Americas, the United States and our armed forces will remain the greatest force for freedom and security and peace that the world has ever known.  That is your legacy.  That is what we have to protect, and that is what we have to defend.  (Applause.)  


	And let me say this:  No one -- no one -- has given more for our freedom and our security than our Gold Star families.  (Applause.)  Michelle and I have spent countless hours with them. We have grieved with them.  There’s a reason why, last week in Philadelphia, I was humbled to be introduced by Sharon Belkofer from Ohio, a Gold Star mom whose son, Tom, a lieutenant colonel in the Army, gave his life in Afghanistan.  I requested Sharon to introduce me, because I understood that our Gold Star families have made a sacrifice that most of us cannot even begin to imagine.  They represent the very best of our country.  (Applause.)  They continue to inspire us every day, every moment. They serve as a powerful reminder of the true strength of America.  We have to do everything we can for those families, and honor them, and be humbled by them.


	DAV, I know that your service has also been defined by another battle.  This is a group that understands sacrifice.  (Applause.)  You’ve been defined by the battle here at home to persevere through wounds and disabilities.  I think of a veteran from Iraq who lost her arm but who said she decided to focus “not on what I had lost, but on what I still had.”  I see that same spirit in you.  Maybe it was there in the hospital bed, fighting for your life, you learned what it really means to have faith.  Maybe it was during rehab, learning how to live without a leg, or both, you learned what it really means to persevere.  


	About a month ago, I went to Walter Reed -- I do this periodically -- and was in the rehab unit watching some of these folks work out.  And I decided -- you might have seen this -- I was doing some pushups with them and -- (laughter) -- trying to keep up with them.  And I was sweating and getting all tired.  (Laughter.)  They took it easy on me.  (Laughter.)  But it gave me a sense of -- just a small sense of what perseverance really means.


	Maybe it was during the night when the memories came rushing back -- and you summoned the courage to reach out and get help and stay strong.  And I was proud to help recognize your patriotism and resilience in the heart of our nation's capital when we dedicated the American Veterans Disabled For Life Memorial.  (Applause.)  


	This organization shows us, shows this nation, what it means to be strong.  But as strong as you are -- and nobody is stronger than our disabled vets -- I know you didn't make this journey alone.  You're here because of the love and support of your families and your caregivers and your neighbors and your communities and your fellow veterans.  (Applause.)  They were the shoulder you leaned on, who carried you when you couldn't walk, who picked you up when you stumbled, who celebrated your victories with you, who sometimes just made you laugh and reminded you how good life can be.  


	And that brings me to what I want to talk about here today. For more than two centuries, this country that we love hasn’t just endured; we have thrived.  We have overcome challenges that would have broken a lesser nation.  And not thanks to any one person or one group of people, but because, like you learned in the military, we’re all one team.  


	We believe in taking care of each other, and in lifting each other up, and leaving no one behind, and in meeting the collective responsibilities that we can only meet together:  The security of our nation.  The education for our children.  Dignity for our seniors.  Equal rights for all of our citizens.  Health care -- which is now a right for everybody.  And the care and well-being of our veterans and your families.  That is a responsibility for all of us, not just a few.  We all have to do our part.  


	And as I've said before, America’s commitment to our veterans is not just lines in a budget.  And it can't be about politics.  It’s not even really about policy.  Our commitment to our veterans is a sacred covenant.  And I don't use those words lightly.  (Applause.)  It is sacred because there's no more solemn request than to ask someone to risk their life, to be ready to give their life on our behalf.  It's a covenant because both sides have responsibilities.  Those who put on the uniform, you took an oath to protect and defend us.  While the rest of us, the citizens you kept safe, we pledged to take care of you and your families when you come home.  That's a sacred covenant.  That's a solemn promise that we make to each other.  And it is binding.  And upholding it is a moral imperative.  (Applause.) 


	And at times, our nation has not always upheld this covenant.  Our Vietnam vets, they sure know this.


	AUDIENCE MEMBERS:  Yes!


	THE PRESIDENT:  When you came home, you deserved better.  (Applause.)  Veterans who at times have struggled to get care at the VA, you deserve better, too.  (Applause.)  If there’s ever a breach in the covenant, then leaders in this country have to work hard to regain trust.  


	That's what Bob and so many hardworking people at the VA are doing.  But upholding this covenant has to be the work of all of us.  It's not just the VA's job.  It's everybody's job. Government has to deliver the care and benefits and support that you have earned.  Veteran service organizations have to hold us accountable and be our partners, like the 1.3 million members of the DAV are doing every day.  And citizens have to step up, too 
	-- which is why Michelle and Dr. Jill Biden, through Joining Forces, have rallied the American people to honor and support our military families and our veterans.


	Now, we've got a lot more work to do.  But working together over these past eight years, we've delivered real progress for our veterans.  And we can’t let up.  It's not a reason for complacency, but we should understand that when we really put our sweat and tears and put our shoulder to the wheel, we can make things better.  


	About 200,000 servicemembers are becoming veterans every single year.  And America is going to have to be there for you for a lifetime in five important ways.  


	Number one, we have to keep fighting for the resources you need.  Now, since I took office, we’ve made historic increases in veterans funding -- the biggest boost in decades.  That's a fact. (Applause.)  And I’ve proposed another increase for next year.  (Applause.)  So altogether, during my presidency, we will have increased funding for veterans by more than 85 percent.  (Applause.)  


	With advance appropriations, we’re protecting veterans’ health care from the annual Washington budget battles.  But I do have to point this out -- Republicans in Congress have proposed cutting my VA budget.  And when they return in the fall, they should pass the budget our veterans need -- and fund it, fully.  Don’t just talk about standing with veterans.  Don't just talk about me.  (Laughter.)  Do something to support our veterans.  That's what you need to do.  (Applause.)


	Number two, we’ve got to keep fighting to deliver the health care you’ve been promised.  Today, more of our Vietnam vets are getting your disability benefits because of your exposure to 
	Agent Orange.  That's a change that we made.   So, too, for our Desert Storm veterans, because of the illnesses tied to the Gulf War.  Those are changes we made.  Altogether, we’ve made VA benefits available to more than 2 million veterans who didn’t have them before.  (Applause.)


	Let’s face it, sometimes folks don't know that, but it's a fact.  And I have to say, thanks to the Affordable Care Act -- Obamacare -- (applause) -- veterans not covered by the VA now have access to quality, affordable health care.  And insurance companies can’t discriminate against you because of preexisting conditions like post-traumatic stress.  (Applause.)  And more veterans are gaining access to health insurance. 


	So we need to keep making it easier to access care.  That’s why we recruited some of the best talent from Silicon Valley and the private sector.  And in one of their first innovations, veterans can now finally apply for VA health care anytime, anywhere, from any device, including your smartphone -- simple, easy, in as little as 20 minutes.  Just go to Vets.gov.  The days of having to wait in line at a VA office, or mailing it in -- those days are over.  (Applause.)  We're finally moving into the 21st century when it comes to helping our veterans.  It's about time.  (Applause.) 


	We’re reaching more veterans, including rural veterans, with telemedicine -- so you can see someone at the VA without leaving your home.  We now have a designated women’s health provider at all VA clinics -- (applause) -- to make sure our women’s veterans get the tailored care, and the dignity and the respect that you deserve.  (Applause.)    


	And for our disabled vets, we have increased funds for prosthetics, eliminated co-pays if you’re catastrophically disabled, made progress on concurrent receipt so more severely disabled retirees can now receive your military retired pay and your VA disability benefits.  (Applause.)  And we’re doing more than ever to make sure your devoted families and caregivers get the skills and support they need to stay strong as well. 


	And here, I want to thank veterans across our country for being part of another mission -- our precision medicine initiative to revolutionize health care with treatments that are tailored for each patient.  As of today, more than 500,000 veterans -- maybe some of you -- have stepped forward and donated your health and genetic data for research, which brings us halfway to our goal of one million veterans that are doing so.  


	And what this does is it gives us a better understanding of genetics, which will allow us to improve treatments for things like traumatic brain injury and post-traumatic stress, and diabetes, and cancer.  And that won’t just help veterans.  It will help all Americans.  And it's just one more example of how our veterans keep serving our country even after they come home. (Applause.)  


	We need to keep improving mental health care.  I’ll never forget the soldiers I met at Fort Bliss.  They were proud of their service, but they were struggling with issues like post-traumatic stress.  So, for veterans with PTS, we made it easier for you to qualify for the VA care that you need -- no matter when you served.  We’ve increased funding for veterans mental health care by more than 75 percent -- billions more dollars.  More awareness and outreach -- because we have to end any shame or stigma that comes with going and getting help.  (Applause.)


	We've put in place more clinicians, more counselors, more peer support -- veterans helping veterans.  More research -- $100 million for new approaches to PTS and TBI.  And today, we’re delivering more mental health care to more veterans than ever.  We are saving lives.  (Applause.)  


	But when too many veterans still aren’t getting the care that they need, we all have to be outraged.  We all have to do better.  And when 20 veterans a day are taking their own lives -- that is a national tragedy.  We all have to do better.  Most of those 20 vets taking their lives each day are not in the VA.  But we know that when vets do get VA care, they’re more likely to survive.  So we need to get more vets connected to the VA.  And when you have an urgent need for mental health care, you shouldn’t have to wait days, you shouldn’t have to wait weeks -- you should get those services the very same day.  (Applause.)  


	And Congress can help by providing the funding and flexibility we need to hire highly qualified mental health professionals.  And medical schools can help us recruit and train more psychiatrists.  And every American, military and civilian, can help, as well, by learning those Five Signs that somebody is hurting, so we can reach out and help our veterans stay strong.  We’re one team.  One American family.  When any member of our family is suffering, we’ve got to be there for each other. 


	Now, we also need to keep fixing the problems that came to light -- long wait times, veterans denied care, people manipulating the books -- inexcusable.  I know Bob gave you an update, but I want to repeat -- we’ve hired thousands more doctors, nurses, staff; opened more clinical space.  And, with the Choice program, we’re helping more veterans get care outside of the VA.  It all adds up to millions more appointments, delivering more benefits to more veterans than ever before.  That is progress.  


	But even as we improve access, more veterans than ever are seeking care.  So we’re putting more and more resources in, but you’ve got more and more demand for care at the same time.  And this surge in demand means there are now more veterans waiting for appointments, even though we’ve done a lot more.  So I know I’m not satisfied.  Bob is still not satisfied.  And we will not let up.  Bob and his new leadership team are going to keep pushing to transform the VA, and he will keep holding people accountable.  (Applause.)  This is somebody who cares deeply about our veterans getting what they deserve and what they have earned.  (Applause.)    


	And when whistleblowers expose misconduct, they need to be protected, not punished.  (Applause.)  We need Congress to make it easier for the VA to help veterans get care in your communities.


	But I have to say -- here’s one thing I want to be very clear about -- here’s one thing we will not do:  We cannot outsource and privatize health care for America’s veterans.  (Applause.)  Now, there are folks who keep pushing this.  They don’t always come out and say the word “privatize,” but you read what they say, that’s what they mean.  And these radical proposals would begin to dismantle the VA health care system that millions of veterans depend on every day.  And that would hurt veterans.  


	Study after study shows that in many areas, like mental health, the quality of care at the VA is often better than in private care.  So let’s listen to our veterans, who are telling us:  Don’t destroy VA health care.  Fix it and make it work, but don’t break our covenant with our veterans.  (Applause.)  


	This brings me to the third area where we have to stay focused.  We have to keep cutting the disability claims backlog. Now, from its peak, we’ve slashed that backlog by nearly 90 percent.  My Chief of Staff and I -- there was a chunk of time when that backlog was high where, every day, no matter what else was happening around the world, he and I, we’d take these walks around the South Lawn just to keep our exercise, keep our steps up.  And every day, we talked about, how are we going to get that backlog down.  And each week, we’d look and see what kind of progress we’re making.  That’s how we reduced it by 90 percent.  


	The backlog is now lower than when I came into office, even though there are a lot more people who are eligible for claims.  And claims decisions are more accurate the first time.  (Applause.)  


	And on both these fronts, we’re keeping at it.  But as we all know, when veterans appeal a decision, you’re put into an appeals system that right now is broken.  (Applause.)  And you shouldn’t have to fight for years to get a straight answer.  Now, we’ve proposed major reforms, and I want to thank the DAV and all the other veterans’ groups for raising your voice on this.  We’ve got to keep up the pressure.  Congress needs to pass comprehensive reform of the claims appeals process -- (applause) -- because if we don’t fix the appeals process, even when we get the backlog down on the original claim, too many folks are waiting on the backend.  We’ve got to fix it.  And we can.  But we’re going to have to push Congress.  And I don’t know if you’ve noticed, that’s hard.  (Laughter.)    


	Fourth, we’ve got to keep fighting for the dignity of every veteran.  And that includes ending the tragedy, the travesty of veterans’ homelessness.  (Applause.)  This is something that, within my administration, we’ve said this is all hands on deck, across government.  Everybody has got to be involved in this.  And with Joining Forces, Michelle and Jill have helped galvanize hundreds of mayors and communities across the country.  Two states, Virginia and Connecticut, as well as 27 cities and towns across the country have effectively ended veteran homelessness.  (Applause.) 


	So, today, I can announce that, nationally, we have now reduced the number of homeless veterans by 47 percent -- nearly half.  (Applause.)  We have just about cut veterans’ homelessness in half.  We’ve helped bring tens of thousands of veterans off the streets.  But we’re not slowing down.  We’re going to keep up the momentum.  This fall, Michelle will bring our partners from across the country together at the White House to share best practices to figure out what has worked, what hasn’t worked.  Because we will not stop until every veteran who fought for America has a home in America.  This is something we’ve got to get done.  (Applause.)  


	And finally, we’ve got to keep fighting to give our troops and veterans and your families every opportunity to live the American Dream that you helped defend.  With our overhaul of the transition assistance program, hundreds of thousands of departing servicemembers and their spouses have received training to plan their next career, and find a job or start a business.  We expanded the Post-9/11 G.I. Bill to reservists and National Guard members and families, including Gold Star spouses and children.  And then we expanded it to vocational training and apprenticeships.  


	We’ve empowered veterans with new tools to find the schools that are right for you, or to get the support you need to succeed on campus; to make sure you don’t get ripped off; to cap your student loans; to make sure you and your families get in-state tuition—now, which is true now in all 50 states.  (Applause.)  And so far, we have helped more than 1.6 million veterans and their families realize their dream of an education, an investment in you and America that will help keep us strong and keep paying off for generations to come.  (Applause.)  


	So we’re doing more to help you find jobs worthy of your incredible talents.  Because if you could lead a team, and run logistics and manage a budget, or save a life in a warzone, you can sure as heck can do it right back here at home.  (Applause.)


	I called for states to recognize the training and skills of veterans when issuing credentials for civilian jobs, licensing.  Now all 50 states do it.  Before, less than half the states made it easy for military spouses to get credentials and licenses.  Today, all 50 states do it.  (Applause.)  Starting this fall, we’ll close loopholes to protect our troops and military families from predatory pay-day lenders.  (Applause.)   


	So, today, all across America, more veterans are at work, on the job, beginning the next chapter of your service to our country.  Veterans who are physicians and nurses have been hired by community health centers.  Cities and towns are hiring veterans as teachers and police officers, firefighters and first responders.  Because we made it a priority in the federal government -- hiring hundreds of thousands of veterans, including disabled veterans -- nearly one in three federal workers is now a veteran.  


	I challenged America’s companies to hire veterans.  And then, in case they weren't listening to me, I sicced Jill and Michelle on them -- (laughter and applause) -- through Joining Forces, and companies now have hired or trained more than 1.2 million veterans and military spouses.  (Applause.)  So, all told, we’ve cut veterans unemployment by more than half, down to 4.2 percent, which is actually lower than the already low national average.  (Applause.)  And it’s way down for Post-9/11 veterans, too.  (Applause.)  It’s one of the reasons we’ve been able to help more than 3.6 million veterans buy or refinance a home of their own.  


	So I’m going to keep saying to every company in America, if you want talent, if you want dedication, if you want to get the job done, then hire a vet!  (Applause.)  Hire a military spouse! (Applause.)  They know how to get the job done.  They don't fool around!  (Applause.)


	So, DAV, we’ve made a lot of progress.  It's not always focused on -- because, understandably, the news a lot of the time focuses on what's still not working.  That's okay.  That keeps us on our toes, keeps us working.  But every once in a while, it's good to remember the progress we've made, because that tells us when we focus on it, we can do right by our veterans.  And as this new generation of veterans joins your ranks, we've got to keep on stepping up our game, giving veterans the resources you need, transforming the VA, delivering the health care you've earned, reducing the backlog, reforming appeals, standing up for your dignity, and helping you share the American dream.


	And I know we can -- because over the past eight years, I’ve seen the spirit of America, and I have seen time and time and time again the strength of our veterans, the unbreakable will of our disabled vets.  You teach us better than anybody that we may take a hit sometimes, we may get knocked down, but we get back up.  We carry on.  (Applause.)  And when we take care of each other and uphold that sacred covenant, there is nothing we cannot do.   


	Like that soldier I’ve told you before -- Army Ranger Veteran Cory Remsburg, nearly killed in Afghanistan, who learned to talk again and walk again, and who recently stood up and walked into the Oval Office and shook my hand.  (Applause.)  We all have to keep on rising.  


	Like Medal of Honor Recipient Staff Sergeant Ty Carter, who struggled with post-traumatic stress, and who’s now helping others stay strong.  Troops, veterans, civilians -- we all have to keep on healing.  


	Like the wounded warriors and disabled vets who are out there running and jumping and swimming and biking and climbing, including Marine Corps Veteran Charlie Linville, who just became the first combat amputee to reach the top of Mount Everest.  We all have to keep striving.  (Applause.)


	Like the veterans taking care of each other, including here at the DAV -- Army Veteran Oscar Olguin; Navy Reserve Veteran Charity Edgar; Marine Corps Veteran Carmen McGinnis -- who says helping veterans “gives me a sense of purpose.”  That’s something we all have to recognize.  We all have to keep on serving.


	Like Air Force Technical Sergeant Jason Miller, who considered taking his own life, but who wrote me a letter -- and after I put him in touch with Team Rubicon, went to work rebuilding communities after disasters, found a new purpose in life -- well, we all have to keep building this country we love.  
	And like the ranks of our military and our veterans -- whether they are black or white, or Latino or Asian or Native American, or they are young or old, whether they are gay or straight, whatever their faith, men, women, Americans with disabilities -- we have to keep on uniting as one team.  As one people.  As one nation.  (Applause.)  


	That’s what you have taught us.  That’s what you are an example of.  The Disabled Veterans of America know what it means to be one team.  We draw inspiration from you.  I am grateful for everything that you have done for this country.  I am grateful for having had the opportunity to work with you.  


	God bless you.  Thank you for your service.  (Applause.)  Thank you for your sacrifice.  Thank you for your patriotism.  We honor and appreciate you.  God bless our veterans and God bless the United States of America.  


	Thank you very much.  (Applause.)


	END
	2:19 P.M. EDT

