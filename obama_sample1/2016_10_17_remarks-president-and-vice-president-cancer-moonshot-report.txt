
	Oval Office


	2:54 P.M. EDT


	THE PRESIDENT:  Well, as many of you know, at my final State of the Union, in addition to talking about the progress we had made on the economic front, some of the challenges and opportunities that we saw internationally, I announced my intention to set up a Cancer Moonshot -- the notion being that given the incredible breakthroughs that we've seen in medicine, the potential that arises out of cracking our genetic code, that we now have the capability to accelerate progress on the disease that's plagued mankind for years. 


	And in invoking this idea of a moonshot, what I hoped to be able to galvanize the country around was the same sense of urgency and an all-hands-on-deck approach, where everybody pulled together -- commercial drug companies, government agencies, philanthropies, organizations like the American Cancer Society, patients organizations -- all to look at where, if we really put our shoulder behind the wheel, where can we make the biggest impact as quickly as possible.


	And to make this thing work, I could not think of somebody who is better to be in the mission control pit than my Vice President, Joe Biden, and our outstanding Second Lady -- what's the phrase we use?


	DR. BIDEN:  Captain of the Vice Squad.  (Laughter.)


	THE PRESIDENT:  Captain of the Vice Squad -- that's what it is.  (Laughter.)  I knew there was a phrase there.  (Laughter.)


	I couldn't think of better folks to make this thing work than Joe and Jill.  Now, part of it obviously is because of how profoundly they've been touched by the disease.  But the truth is, all of us have.  As some of you know, my mother died of cancer when she was younger than I am today.  And there's not a family that, in some way, has not been impacted.


	But given Joe and Jill's passion for the issue but also the incredible organization skills that Joe has shown in the past in mobilizing things like the Recovery Act, I thought that he was ideally suited for this project.  He energetically and enthusiastically took on the challenge.  Jill energetically and enthusiastically joined him.  And what we're now doing is releasing a report -- the results of the last eight months of really intensive work by Joe and Jill, but also a terrific team.  And what they've done is to mobilize researchers, scientists, doctors, hospitals, tech companies, as well as philanthropies and patients advocacy organizations, and traveled around the country and internationally in order to really figure out, how are we going to get this thing to achieve a serious impact?


	As Joe will detail, what we've discovered is that the boundaries of medical knowledge around cancer, the fact that we now understand that what we used to lump together as one cancer might end up being 10 different cancers; the fact that we now have potential ways of treating the disease that don't just involve surgeons or people applying radiation or chemotherapy, but we're having all sorts of disciplines now participating; our ability to identify early people who may have a proclivity for a certain kind of cancer and take preventive actions faster; the capacity of big data to analyze cohorts and our ability to now start our pooling together hundreds of thousands or millions of genetic samples that rapidly accelerate our ability to engage in research -- all these things have been the subject of incredible work by this team and Joe and Jill. 


	And so although we’re going to be leaving soon, what I think we’re going to be able to leave behind is a architecture and a framework for organizing these efforts over the next several years.  And we’re already beginning to see results as people across disciplines who previously were working in isolation are now joining together and realizing we’ve all got one objective.


	And so I could not be prouder of the work that Joe, Jill and this team have done.  I gladly am going to be accepting this report, and, more importantly, I’m looking forward not only to laying the ground work for the next administration to pick up the baton and run with it, but I know that Joe and Jill and I and Michelle will all continue to be involved after we’ve left this office in making sure that this works.


	So thank you, Joe, for the great work.


	THE VICE PRESIDENT:  Well, thank you, Mr. President.  And thank you -- I’m going to give you the report.  I want to --


	THE PRESIDENT:  Give us the executive summary.


	THE VICE PRESIDENT:  The executive summary.


	THE PRESIDENT:  The report is much fatter.  (Laughter.)  And I’m looking forward to all of you reading it.


	THE VICE PRESIDENT:  Mr. President, thank you for the trust you gave Jill and me to do this.  And as you said to me early, early on, it's clear, folks, that the vice-presidency has no power, it’s all reflective of the President’s confidence.  And the fact that you made clear and you laid out how you expected the administration to respond has given me very wide authority to coordinate all the agencies of the government that had any possible impact on this fight.  And they’ve been incredibly cooperative.


	Mr. President, in 1971, when Richard Nixon declared the war on cancer, he had no army, he had no plans, he had no weapons.  And the culture, the medical culture that surrounded the beginning of that effort is fundamentally different than it is today.  Now we have, after 40 years of enormous accumulation of data, research, and brilliant minds who have devoted their attention to this, we’ve reached an inflection point, Mr. President.  This is what we learned when we tried to figure out, like every family does, how to deal with -- cancer.


	What we found out is, even five years ago, Mr. President, immunotherapy was viewed as some voodoo science -- that is, how do you get the immune system to be energized and triggered to go out and kill the cancer in your body.  Well, there’s enormous breakthroughs being made.  But before, oncologists never worked with immunotherapists.  They didn’t work with virologists.  They didn’t work with chemical engineers and biological engineers.


	So we started with our administration, Mr. President.  And in addition to the obvious candidates to be engaged in this, and have been engaged -- from NIH to the National Cancer Institute to HHS -- we found out that there were other areas that were incredibly important -- for example, NASA is now involved.  Why?  Because radiation, when it’s used to kill what’s left over from whatever the tumor has excised, it does enormous damage, sometimes more damage than the cancer.


	And so there’s new technologies.  Nobody has better technology than NASA in how to deal with radiation, because that’s what all our astronauts are constantly bombarded with.  And so we’ve also gotten involved -- the Department of -- the Patent Office to move quickly.


	So my point is, they have all cooperated and brought in all these disciplines.  In the private sector, Mr. President, the same thing is beginning to happen.  At first it was not on my watch.  But now, for example, Mr. President, drug companies are realizing that taking more than one of their therapies or one of their drugs from different companies in combination may have a profound impact on curing and/or drastically damaging the cancer.  But before, they wouldn’t share it.


	But so what we came up with is essentially a free licensing agreement.  There are all these drug companies are putting in all their drugs that they’re experimenting with, ones that they have yet to get patents on -- and saying, have at it, gentlemen and ladies, the researchers.  And they know that if something comes from it, it’s like when you stick money in a jukebox, you play a song -- you're going to play a song by Beyoncé or whomever -- there's already a licensing agreement.  She gets a share of what gets dropped in the box.  The same thing is happening.  


	So, Mr. President, the private sector stepped up as well.  You have IBM having contact with Greg Simon, who heads this effort for me, and saying, we're prepared to work with the VA hospitals using Watson to go out and analyze all the tumor biopsies that are out there and find patterns.  And it's the single largest hospital in the world, with the most cancer patients assembled any one place.  We’re going to find patterns of what causes this disease.  Microsoft is engaged with us now, and providing -- and Amazon -- space in the cloud for genomic information, for the genomic data.


	So, the point is, Mr. President, that the fundamental thing I come away with is that there is a need for a greater sense of urgency because there is -- there are available answers now to some cancers, and there is enormous opportunity in sharing data.  You have our Department of Energy being able to do a million-billion calculations a second.  If you could aggregate all the cancer data in one spot, imagine the capacity of being able to analyze all the -- to find patterns and why a particular therapy works on you but not on me, works on one patient but not another.  So there’s real excitement. 


	And the second thing, Mr. President, that I came away with -- and I’m going to be leaving here and going over -- there’s 200 oncologists, researchers, virologists, philanthropists, et cetera, over in the OEB.  And, Mr. President, I’m going to release the task force report that you authorized me to organize.  And it has another 35 specific recommendations beyond the 45 that were already put forward.


	But the essence of it all gets down to one thing:  We need a culture in 2016 that matches the accumulation of information and data we now have that did not exist when this culture was set up in 1971.  That’s what everybody seems to be arriving at, and it’s really exciting, Mr. President. 


	I am confident, absolutely confident that we’ll be able to accomplish in the next five years what otherwise would have taken us 10 years.  I am confident we’re going to find new prevention techniques out there.  For example, the HPV virus now in fact is able to be -- there’s a vaccination you can have to prevent our children from getting cervical cancer, throat cancer, a whole range of other things.


	So, Mr. President, in conclusion, I look forward to the day when your grandchildren and my grandchildren and their children show up at the office to get their physical to start school and get a shot for measles and they get a vaccine that affects significant causes of cancer.  We are very close to reaching that point and finding out what is the fundamental root cause of what makes a gene not turn off, a rogue gene, a cancer gene continue to eat up parts of the body.  And I’m really optimistic, Mr. President.


	And thank you again for the confidence in letting me lead this effort.  And like you said, in my case, I’m going to devote the rest of my life to working on this.  And I think we’re perilously close to making some gigantic progress.


	THE PRESIDENT:  Well, you've done a great job.  Jill has done a great job. 


	Two other things that I just want to note.  Number one, because of Joe's really active engagement with Congress, this has strong bipartisan support.  And we are hopeful that the already significant funding stream that goes to NIH and other government agencies is significantly supplemented and directed by the work that Joe and Jill have done.


	Secondly, this all comes down to how does it impact patients.  And so Joe, I think, has done a great job in engaging people who are going through battling cancer right now, and finding ways that we're not just coming up with cures, but we're also making sure that these systems are set up so that they're easier for people to access, so that there's a broad, diverse group of people who are subject of studies so that -- because, as Joe said, what may work for an Asian American man may not be the exact same treatment or approach that might work for a young Caucasian woman.  And that means that we've got to bring in data sets and engage communities across America, and not just a sliver.


	And the fact that this team has done so much work to reach out to every corner of the country is reflective of the fact that this is an issue that binds us together.  This is a common enemy.  And we’re really lucky to have such a great couple of generals here to help us --


	THE VICE PRESIDENT:  Mr. President, let me make one concluding statement.  Two reports you’re asking for.  One is a vision statement -- where we go from here, what we have to do.  The second is a much thicker report that I’m going to be delivering to the task force that lays out all the things we recommend being done right now.


	And so, again, Mr. President, thank you.


	THE VICE PRESIDENT:  Good.  Great job.  Thank you, everybody. 


	BILL PLANTE:  Mr. President, [inaudible]?


	THE PRESIDENT:  We're talking about cancer today, Bill. Thank you, sir.  Thank you


	Appreciate you.  Thank you, guys.


	Think we can take at least about a five minute break from that?  Thank you.


	END  

