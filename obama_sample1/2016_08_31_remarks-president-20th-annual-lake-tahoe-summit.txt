
	Lake Tahoe, Nevada


	2:26 P.M. PDT


	THE PRESIDENT:  Hello, Tahoe!  This is really nice.  I will be coming here more often.  (Applause.)  My transportation won't be as nice -- (laughter) -- but I'll be able to spend a little more time here.


	First of all, I want to thank Harry Reid.  (Applause.)  And because he's a captive audience, he doesn’t usually like people talking about him, but he's stuck here, and so I'm going to talk about him for a second. 


	Harry grew up in a town that didn’t have much.  No high school, no doctor's office.  Searchlight sure didn’t have much grass to mow or many trees to climb.  It didn’t look like this.  So when Harry discovered a lush desert oasis down the road called Piute Spring, he fell in love.  And when Harry met Landra, the love of his life, he couldn’t wait to take her there.  But when he got to the green spring that Harry remembered, he was devastated to see that the place had been trashed.  And that day, Harry became an environmentalist -- and he’s been working hard ever since to preserve the natural gifts of Nevada and these United States of America.  (Applause.) 


	So Harry has protected fish and wildlife across the state.   He helped to end a century-old water war.  He created Nevada’s first and only national park.  Right after I took office, the very first act Harry's Senate passed was one of the most important conservation efforts in a generation.  We protected more than 2 million acres of wilderness and thousands of miles of trails and rivers.  That was because of Harry Reid.  (Applause.)  Last summer, thanks to Harry Reid's leadership, we protected more than 700,000 acres of mountains and valleys right here in Nevada, establishing the Basin and Range National Monument.


	Two decades ago, the senator from Searchlight trained a national spotlight right here, on Lake Tahoe.  And as he prepares to ride off into the sunset -- although I don’t want him getting on a horse -- (laughter) -- this 20th anniversary summit proves that the light Harry lit shines as bright as ever.  (Applause.)  Now, in a few months, I’ll be riding off into that same sunset.  (Laughter.) 


	AUDIENCE:  Booo --


	THE PRESIDENT:  No, it's true.  It's okay.  I mean, I'm still going to -- I'm going to be coming around, I told you.  I just won't have Marine One.  (Laughter.)  I'll be driving. 


	But let me tell you, one of the great pleasures of being President is having strong relationships with people who do the right thing.  They get criticized, they've got a tough job, but they get in this tough business because ultimately they care about this country and they care about the people they represent.  And that is true of Dianne Feinstein.  (Applause.)  That is true of Barbara Boxer.  That is true of the outstanding governor of California, Jerry Brown.  (Applause.)  That's true of our outstanding folks who work for the Department of Interior and work for -- who help look after our forests and help look after our national parks that help manage our water and try to conserve the wildlife and the birds, and all the things that we want to pass on to the next generation. 


	And so I'm going to miss the day-to-day interactions that I've gotten.  And I'll miss Harry, even though he's not a sentimental guy.  (Laughter.)  We were talking backstage -- anybody who's ever gotten on the phone with Harry Reid, you'll be making conversation, and once he's kind of finished with the whole point of the conversation, you'll still be talking and you realize he's hung up.  (Laughter.)  And he does that to the President of the United States.  (Laughter.)  And it takes you, like, three of four of these conversations to realize he's not mad at you, but he doesn’t have much patience for small talk.


	But Harry is tough.  I believe he is going to go down as one of the best leaders the Senate ever had.  (Applause.)  I could not have accomplished what I accomplished without him being at my side.  So I want to say publicly, to the people of Nevada, to the people of Lake Tahoe, to the people of America:  I could not be prouder to have worked alongside the Democratic Leader of the Senate, Harry Reid.  Give him a big round of applause.  (Applause.)  


	So, it’s special to stand on the shores of Lake Tahoe.  I’ve never been here. 


	AUDIENCE:  Oooh --


	THE PRESIDENT:  No -- it’s not like I didn’t want to come.  (Laughter.)  Nobody invited me.  (Laughter.)  I didn’t know if I had a place to stay.  So now that I’ve finally got here, I’m going to come back.  (Applause.)  And I want to come back not just because it’s beautiful, not just because --


	AUDIENCE MEMBER:  We love you!


	THE PRESIDENT:  Well, not just because I love you back.  (Laughter.)  Not just because The Godfather II is maybe my favorite movie.  (Applause.)  As I was flying over the lake, I was thinking about Fredo.  (Laughter.)  He's tough.  But this place is spectacular because it is one of the highest, deepest, oldest and purest lakes in the world.  (Applause.)  It’s been written that the lake’s waters were once so clear that when you were out on a boat, you felt like you were floating in a balloon.  Unless you were Fredo.  (Laughter.)  It’s been written that the air here is so fine, it must be “the same air that the angels breathe.” 


	So it’s no wonder that for thousands of years, this place has been a spiritual one.  For the Washoe people, it is the center of their world.  (Applause.)  And just as this space is sacred to Native Americans, it should be sacred to all Americans.


	And that’s why we’re here:  To protect this pristine place.  To keep these waters crystal clear.  To keep the air as pure as the heavens.  To keep alive Tahoe’s spirit.  And to keep faith with this truth -- that the challenges of conservation and combatting climate change are connected, they’re linked. 


	AUDIENCE MEMBER:  (Inaudible.)


	THE PRESIDENT:  Okay.  I'm sorry, I --


	AUDIENCE MEMBER:  (Inaudible.) 


	THE PRESIDENT:  I gotcha, okay.  I gotcha.  Thank you, that’s a great banner.  I’m about to talk about it though, so you’re interrupting me.


	Now, I was going talk about climate change and why it’s so important.  You know, we tend to think of climate change as if it’s something that’s just happening out there that we don’t have control over.  But the fact is that it is manmade.  It’s not “we think” it’s manmade.  It’s not “we guess” it’s manmade.   It’s not “a lot of people are saying” it’s manmade.  It’s not “I’m not a scientist, so I don’t know.”   You don’t have to be a scientist.  You have to read or listen to scientists -- (applause) -- to know that the overwhelming body of scientific evidence shows us that climate change is caused by human activity.  And when we protect our lands, it helps us protect the climate for the future.


	So conservation is critical not just for one particular spot, one particular park, one particular lake.  It’s critical for our entire ecosystem.  And conservation is more than just putting up a plaque and calling something a park.  (Applause.)   We embrace conservation because healthy and diverse lands and waters help us build resilience to climate change.  We do it to free more of our communities and plants and animals and species from wildfires, and droughts, and displacement.  We do it because when most of the 4.5 million people who come to Lake Tahoe every year are tourists, economies like this one live or die by the health of our natural resources.  (Applause.)  We do it because places like this nurture and restore the soul.  And we want to make sure that’s there for our kids, too.  (Applause.)


	As a former Washoe Tribe leader once said, “The health of the land and the health of the people are tied together, and what happens to the land also happens to the people.”


	So that’s why we’ve worked so hard -- everybody on this stage, Harry’s leadership, the work we’ve done in our administration -- to preserve treasures like this for future generations.  And we’ve proven that the choice between our environment, our economy, and our health is a false one.  We‘ve got to strengthen all of them together. (Applause.) 


	In the 20 years since President Clinton and Senator Reid started this summit, federal, state, and local leaders have worked together to restore wetlands and habitats, improve roads, reduce pollution, and prevent wildfires.  And that last point is especially important because of the severe drought that all of you know and you can see with your own eyes.  A single wildfire in a dangerously flammable Lake Tahoe Basin could cause enough erosion to erase decades of progress when it comes to water quality.  And the drought also endangers one of the epicenters of the world’s food production in California.


	A changing climate threatens even the best conservation efforts.  Keep in mind, 2014 was the warmest year on record until, you guessed it, 2015.  And now 2016 is on pace to be even hotter.  For 14 months in a row now, the Earth has broken global temperature records.  Lake Tahoe’s average temperature is rising at its fastest rate ever, and its temperature is the warmest on record. 


	And because climate and conservation are challenges that go hand in hand, our conservation mission is more urgent than ever.  Everybody who is here, including those who are very eager for me to finish so that they can listen to the Killers -- (laughter) -- I’ve only got a few more pages.  Our conservation effort is more critical, more urgent than ever.  And we made this a priority from day one. 


	We, as Harry mentioned, protected more acres of public lands and water than any administration in history.  (Applause.)  Now, last week alone, we protected land, water, and wildlife from Maine to Hawaii -- (applause) -- including the creation of the world’s largest marine protected area.  And, apropos of the young lady’s sign, we've been working on climate change on every front.  We've worked to generate more clean energy, use less dirty energy, waste less energy overall. 


	In my first months in office, Harry helped America make the single largest investment in renewable energy in our history.  Dianne Feinstein and Barbara Boxer have been at the forefront of this.  Jerry Brown has been doing incredible legislative work in his state.  These investments have helped drive down the cost of clean power, so it’s finally cheaper than dirty power in a lot of places.  It helps us multiply wind power threefold; solar power more than thirtyfold.  It’s created tens of thousands of good jobs.  It’s adding to paychecks, subtracting from energy bills.  It’s been the smart and right thing to do.  (Applause.) 


	And then one year ago this month, we finalized a Clean Power Plan that spurs new sources of energy and gives states the tools to limit pollution that power plants spew into the sky.  As I mentioned, last week California passed an ambitious plan to cut carbon pollute.  And, Jerry, I know you agree that more states need to follow California’s lead.  (Applause.)   


	On a national level, we’ve enacted tough fuel-economy standards for cars, which means you’re going to be able to drive further on a gallon of gas.  It’s going to save your pocketbook and save the environment.  We followed that up with the first-ever standards for commercial trucks, vans, and buses.  And as a consequence, during the first half of this year, carbon pollution hit its lowest level in a quarter century.  (Applause.)  And, by the way, during the same time, we’ve had the longest streak of job creation on record.  The auto industry is booming.  There is no contradiction between being smart on the environment and having a strong economy, and we got to keep it going.  (Applause.)


	So this isn’t just a challenge, this is an opportunity.  And today in Tahoe, we’re taking three more significant steps to boost conservation and climate action.  First, we're supporting conservation projects across Nevada to restore watersheds, stop invasive species, and further reduce the risks posed by hazardous fuels and wildfires.  (Applause.)  Number two, we’re incentivizing private capital to come off the sidelines and contribute to conservation, because government can’t do it alone.  Number three, in partnership with California, we’re going to reverse the deterioration of the Salton Sea before it is too late, and that’s going to help a lot of folks all across the West.  (Applause.)


	So, we’re busy.  And from here, I’m going to travel to my original home state of Hawaii, where the United States is proud to host the World Conservation Congress for the first time.  Tomorrow, I’m going to go to Midway to visit the vast marine area that we just create and to honor those who sacrificed their lives to protect our freedom.  (Applause.)  Then I head to China, with whom we’ve partnered -- as the world’s two largest economies and two largest carbon emitters -- to set historic climate targets that are going to lead the rest of the world to a cleaner, more secure future.  (Applause.) 


	So I just go back to that quote by the Washoe elder: “What happens to the land also happens to the people.”  I’ve made it a priority in my presidency to protect the natural resources we’ve inherited because we shouldn’t be the last to enjoy them.  Just as the health of the land and the people are tied together, just as climate and conservation are tied together, we share a sacred connection with those who are going to follow us.  I think about my two daughters.  I think about Harry’s 19 grandchildren.  (Laughter.)  He's been -- yeah, that's a lot of grandkids.  (Laughter.)  The future generations who deserve clear water and clean air that will sustain their bodies and sustain their souls -- jewels like Lake Tahoe.


	And it’s not going to happen without hard work.  It sure is not going to happen if we pretend a snowball in winter means nothing is wrong.  It’s not going to happen if we boast about how we’re going to scrap international treaties, or have elected officials who are alone in the world in denying climate change, or put our energy and environmental policies in the hands of big polluters.  It’s not going to happen if we just pay lip service to conservation but then refuse to do what’s needed.


	When scientists first told us that our planet was changing because of human activity, it was received as a bombshell.  But in a way, we shouldn’t have been surprised.  The most important changes are always the changes made by us.  And the fact that we’ve been able to grow our clean energy economy proves that we have agency, we’ve got power.  Diminishing carbon pollution proves we can do something about it.  Our healing of Lake Tahoe proves it’s within our power to pass on the incredible bounty of this country to a next generation.  (Applause.)  Our work isn’t done. 


	And so after I leave office, and Harry leaves office, and Barbara -- she’s going to be right alongside us, on a slightly smaller horse -- (laughter) -- because she’s got to get up on top of it -- after we’ve all left office, the charge to continue to make positive change is going to be in all of our hands, as citizens.  I always say the most important office in a democracy is the office of citizen.  Change happens because of you.  Don’t forget that.


	Thank you.  God bless you.  God bless the United States of America.  (Applause.)


	                        END                2:47 P.M. PDT


	-----

