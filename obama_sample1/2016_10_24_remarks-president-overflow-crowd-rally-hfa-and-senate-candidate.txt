
	Cheyenne High School


	North Las Vegas, Nevada


	4:33 P.M. PDT


	     THE PRESIDENT:  Hello, Las Vegas!  (Applause.)  How is everybody doing?  (Applause.)  Good?  So I'm sorry that it's a little crowded up in there.  (Applause.)  But I just wanted to let you guys know how much I appreciate you.  I would not be President if it weren't for all the work that so many of you did back in 2008, back in 2012.  (Applause.) 


	     But if we're going to continue all the progress that we’ve made, then we are going to have to make sure that we vote this time out.  (Applause.)  So I need everybody here to not just vote yourselves, but you got to get your friends, your neighbors, your cousins.  If you're not 18 and you can't vote, make sure your parents vote and your cousins vote.  Because that's the only way we're going to be able to continue the progress that we've made.


	     If you care about putting people back to work, then you want Hillary Clinton as President of the United States.  (Applause.)  If you want to make sure that immigration reform gets passed, we've got to have Catherine Cortez Masto in the United States Senate.  (Applause.)  If you want to make sure that we continue to make progress on education and making college affordable, then we've got to have more Democratic members of Congress in the House of Representatives.  (Applause.) 


	     So we got two weeks.  I need you to work as hard as you can.  You know the stakes.  We can't afford the other guy. 


	     AUDIENCE:  Nooo --


	     THE PRESIDENT:  Can't do that.  I'd feel really bad.  (Laughter.)  So this is no joke.  Do not take anything for granted.  I need you all to get out there.  If you do, then I know that we're going to continue to build on the progress we've made the last eight years, and nobody is going to be able to stop the United States of America from making sure that every child in America is able to fulfill its dreams.


	     I love you.  God bless you.  (Applause.) 


	                            END                   4:35 P.M. PDT         

