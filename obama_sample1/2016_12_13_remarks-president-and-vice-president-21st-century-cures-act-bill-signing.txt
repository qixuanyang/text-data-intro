
	South Court Auditorium


	2:54 P.M. EST


	THE VICE PRESIDENT:  Mr. President, it's a lousy club, but I'm proud of you.  We're all proud of you.  


	Mr. President, my Senate colleagues, all the members of Congress who are here and worked so hard to get this bill done today, just let me say that last week I had the honor of presiding, probably for the last time in the United States Senate, over the Senate as, Mr. President, they moved to pass the 21st Century Cures Act.  And as I said, it's probably one of the last times that I will get to preside over the Senate, and maybe one of the most important moments in my career.


	On behalf of the administration, let me thank all the bipartisan leadership here.  I want to make this clear:  This bill would have never occurred, not for some of the -- without the leading voices, Republican voices, in the House and the Senate, as well as Democrats.  It would have never, ever occurred.  And I hope this bodes well for what will come next year, that we're back working together.  This is a consequential piece of legislation that was extremely important and cost a lot of money, and it was done in the lame duck session. 


	Without the true bipartisan support, this piece of legislation would have never occurred, and it's going to help millions of people -- millions of people.  As the President and I talked -- he'll talk about this in greater detail in a moment -- the 21st Century Cures Act is going to harness America's best minds of science, medicine and technology to tackle some of our biggest and most complex health challenges of today.


	The bill commits $6.3 billion over seven years, dealing with opioid addition, precision medicine, and the BRAIN initiative, and mental illness, Alzheimer's disease, and so much more.  But, Mr. President, if you'll excuse as we both have just a done -- a point of personal privilege, I want to thank my colleagues.  Of that $6.3 billion, $1.8 billion will go and be invested in cancer research and care.


	When the President asked me last year at the State of the Union to head the Cancer Moonshoot, we said we were going to ask you all for significant funding increases at the NIH and the National Cancer Institute.  And you all stepped up again, Republicans and Democrats.  As part of the Moonshoot, we set up what's called a Blue Ribbon Panel to review what should be the scientific priorities as we tackle this to try to end cancer as we know it.  We'll try to do in the next five years what ordinarily would take ten years.  


	These priorities include investing in promising new therapies like immunotherapy, using the body's own immune system to target and kill cancer cells; enhancing prevention and detection efforts in every community, regardless of the zip code in which you live; supporting research to improve outcomes for children with cancer, and putting us on a path to turn what is currently a devastating cancer diagnosis into either a chronic disease or an absolute cure.


	And in the process, it will fundamentally, I believe, change the culture of our fight against cancer, and inject an overwhelmingly sense of the urgency, or, as the President often says, the urgency of now.  Because every single moment counts, as Senator Murray and everybody else who's worked on this bill knows.  God willing, this bill will literally, not figuratively, literally save lives.  


	But most of all, what it does -- just this mere signing today, Mr. President, as you know better than I do, gives millions of Americans hope.  There's probably not one of you in this audience or anyone listening to this who hasn’t had a family member or friend or someone touched by cancer.  


	And I want to particularly thank my colleagues, Senator McConnell and Senator Reid, who moved, Mr. President, as you know, to name this section of the bill after our son, and Jill, who's here with me today -- our son, Beau.  (Applause.)  As we used to say in the Senate, a point of personal privilege, Mr. President -- and you know he loved you, and you were wonderful to Beau.  And he spent a year in Iraq, came back a decorated veteran, and he was attorney general of the state, and he never, ever, ever gave up -- nor did we.


	And we had access to some of the best doctors in the world, including the head of the Department of Neuro-Oncology at MD Anderson -- became a great friend of ours -- Dr. Al Yung.  Al, thank you for being here.  But, you know, as I said, we never gave up.  But Jill and I realized that we're not the only family touched by cancer.  And so many are touched who don’t have nearly the support system we've had.  And, Mr. President, you lost your mother, and so many other families in here have lost someone to cancer.  


	And as I said, this legislation is going to give hope.  Every day, millions of people are praying -- praying for hope, praying for time, praying that somehow something will happen just to extend their -- they're not even praying for cures most of the time.  Those of you who are doctors in the audience, how many times have you heard a patient say, Doc, can you just give just three more weeks so I can walk her down the aisle, or, just give me another two months, it's my first grandbaby and I want see him or her born.  It's all a matter of hours, days, weeks, months.  


	And what we're doing here now is -- this is going to accelerate exponentially, in my view, the kinds of efforts we can make right now, things that are at our disposal right now to extend life.


	Ladies and gentlemen, I believe President Obama and my colleagues in the Senate -- as I said, both parties -- were motivated by the same commitment that -- after whom this Moonshot was named.  I mean, President Kennedy had talked about going to the moon.  The problem is, there was only one moon, and there's 200-and-some cancers.  But here's what he said.  Here's what he said.  He said, we are unwilling to postpone.  We all here are unwilling to postpone -- unwilling to postpone another minute, another day.  And doing what we know is within our grasp -- it shows the government at its best, Mr. President, and it shows that our politics can still come together to do big, consequential things for the American people.  I see my friend, Senator Hatch, who I worked with for years and years, had stood up in this.  All junior senators, senior senators -- everyone came together.  


	So Jill and I are proud to stand beside you, Mr. President, as you sign this last law of our administration.  I'm proud to have served with you, Mr. President.  And your absolute commitment to changing the way in which we deal with our health care system is going to make a big difference.  And this particular bill is going to allow people to live, live longer, and live healthier.  But, most of all, Mr. President, I think it gives people hope.  


	So, ladies and gentlemen, I always kid the President that when he asked me to join him on the ticket and my daughter came home at lunch -- she's social worker -- and she said, did he call?  Did he call?  And I said, yes.  She said, you said yes, didn’t you, Daddy?  (Laughter.)  And I said, yes, of course I did.  And she said, this is wonderful.  She said, you know how you're always quoting Seamus Heaney about hope and history rhyming?  And I said, yeah.  She said, this is hope and history.  I'm history, here's hope.  (Laughter and applause.)


	Ladies and gentlemen, thank you.  (Applause.)  


	THE PRESIDENT:  Thank you.  Thank you, everybody.  Thank you.  (Applause.)  Thank you so much.  Thank you.  (Applause.)  Thank you.  Please, have a seat.  Thank you so much.


	Well, welcome to the White House, everyone.  It’s December, so it’s holiday time around here, and we thought it was a good occasion to have one more party.  And this is a celebration worth having.  


	I want to, first of all, thank Joe Biden and Jill Biden, and their entire family, who have been such extraordinary friends to us.  And what a fitting way for us to be able to signify our partnership as our time comes to an end together.  It makes me feel very good.


	I want to thank David and Kate Grubb for sharing their family’s story.  As David said, we have a lot in common, and nothing more than the love of our children, our daughters.  When I first met them in Charleston, their story was, unfortunately, more common than we would have liked.  And I indicated a number of the people on this stage are people who have gone through tough times or have seen their loved ones suffer, either because of opioid addiction or because of cancer; who have bravely shared their story and channeled their passion into increasing the urgency all of us feel around this issue.  


	And so, more than anything, this is a testimony to them, and an extraordinary commemoration of those that they've loved.  So we're very grateful to them.  Please give them a big round of applause.  (Applause.)   


	We’re joined by a whole bunch of members of Congress here today.  And it is wonderful to see how well Democrats and Republicans in the closing days of this Congress came together around a common cause.  (Applause.)  And I think it indicates the power of this issue, and how deeply it touches every family across America.


	Over the last eight years, one of my highest priorities as President has been to unleash the full force of American innovation to some of the biggest challenges that we face.  That meant restoring science to its rightful place.  It meant funding the research and development that’s always kept America on the cutting edge.  It’s meant investing in clean energy that’s created a steady stream of good jobs and helped America become the world’s leader in combatting climate change.  It meant investing in the medical breakthroughs that have the power to cure disease and help all of us live healthier, longer lives.  


	So I started the 2016 State of the Union address by saying we might be able to surprise some cynics and deliver bipartisan action on the opioid epidemic.  And in that same speech, I put Joe in charge of mission control on a new Cancer Moonshot.  And today, with the 21st Century Cures Act, we are making good on both of those efforts.  We are bringing to reality the possibility of new breakthroughs to some of the greatest health challenges of our time.  


	Joe has already indicated some of the scope of the bill, but let me repeat it, because it’s worth repeating.  First, this legislation is going to combat the heroin and prescription opioid epidemic that is ravaging too many families across the country.  This is an epidemic that can touch anybody -- blue collar, white collar, college students, retirees, kids, moms, dads.  I’ve had the chance to meet people from every stage of recovery who are working hard to sustain the progress that they’re making.  And I’ve met parents like the Grubbs, who have worked tirelessly to help a child struggling with addiction. 


	It could not be clearer that those of us called upon to lead this country have a duty on their behalf, that we have to stand by them; that, all too often, they feel as if they’re fighting this fight alone instead of having the community gather around them and give them the resources and the access and the support that they need.


	So today, I could not be prouder that this legislation takes up the charge I laid out in my budget to provide $1 billion in funding so that Americans who want treatment can get started on the path to recovery and don’t have to drive six hours to do it.  It is the right thing to do, and families are ready for the support.  (Applause.)  


	Second, the Cures Act provides a decade’s worth of support for two innovative initiatives from my administration.  The first is the BRAIN Initiative, which we believe will revolutionize our understanding of the human mind.  And when I sign this bill into law, we’ll give researchers new resources to help identify ways to treat, cure, and potentially prevent brain disorders like Alzheimer’s and epilepsy, traumatic brain injury, and more.  


	And we’re also going to support what we’ve called our Precision Medicine Initiative, an effort we started to use data to help modernize research and accelerate discoveries so that treatment and health care can be tailored specifically to individual patients.  This spring, with the help of this legislation, the National Institutes of Health plans to launch a groundbreaking research cohort, inviting Americans from across the country to participate to support the scientific breakthroughs of tomorrow. 


	Number three, the Cures Act improves mental health care.  (Applause.)  It includes bipartisan reforms to address serious mental illness.  It takes steps to make sure that mental health and substance-use disorders are treated fairly by insurance companies, building on the work of my Presidential Task Force.  And it reauthorizes, meaningfully, suicide prevention programs.  Many of these reforms align with my administration’s work to improve our criminal justice system, helping us enhance data collection and take steps so that we’re not unnecessarily incarcerating folks who actually need mental health assistance.  


	Fourth, we’re building on the FDA’s work to modernize clinical trial design so that we’re updating necessary rules and regulations to protect consumers so that they’re taking into account this genetic biotech age.  And we’re making sure that patients’ voices are incorporated into the drug development process. 


	And finally, the Cures Act invests in a breakthrough effort that we’ve been calling the Vice President’s Cancer Moonshot.  And I think the Senate came up with a better name when they named it after Beau Biden.  (Applause.)  


	Joe said Beau loved me.  I loved him back.  And like many of you, I believe that the United States of America should be the country that ends cancer once and for all.  We’re already closer than a lot of folks think, and this bill will bring us even closer, investing in promising new therapies, developing vaccines, and improving cancer detection and prevention.  Ultimately, it will help us reach our goal of getting a decade’s worth of research in half the time.  And as Joe said, that time counts.  


	In this effort, Joe Biden has rallied not just Congress, but he has rallied a tremendous collection of researchers and doctors, philanthropists, patients.  He’s showing us that with the right investment and the ingenuity of the American people, to quote him, “there isn’t anything we can’t do.”  So I’d like everybody to just please join me in thanking what I consider to be the finest Vice President in history, Joe Biden.  (Applause.)  Go ahead and embarrass Joe.  Go ahead.  (Laughter and applause.)  Hey!  


	So we’re tackling cancer, brain disease, substance-use disorders, and more.  And none of this work would have been possible without bipartisan cooperation in both houses of Congress.  A lot of people were involved, but there are some folks who deserve a special shout-out.  That includes Senators Alexander and Senators Murphy.  (Applause.)  Representatives Upton, Pallone, and DeGette, and Green.  (Applause.)  And of course, we couldn’t have gotten across the finish line without the leadership of Nancy Pelosi and Steny Hoyer, who are here -- (applause) -- as well as leaders from both houses, Speaker Ryan, Leaders McConnell and Reid, and Senator Patty Murray.  (Applause.)  Not to mention all the members of Congress who are sitting here that I can’t name, otherwise I’m going to be here too long and I will never sign the bill.  (Laughter.)  But you know who you are.


	I want to thank all of you on behalf of the American people for this outstanding work.  These efforts build on the work that we’ve done to strengthen our healthcare system over the last eight years -- covering preexisting conditions, expanding coverage for mental health and substance-use disorders, helping more than 20 million Americans know the security of health insurance.  Thanks to the Affordable Care Act, it means they have access to some of the services that are needed.  


	I’m hopeful that in the years ahead, Congress keeps working together in a bipartisan fashion to move us forward rather than backward in support of the health of our people.  Because these are gains that have made a real difference for millions of Americans. 


	So this is a good day.  It’s a bittersweet day.  I think it’s important to acknowledge that it’s not easy for the Grubbs to come up here and talk about Jessie.  It’s not easy for Joe and Jill, I know, to talk about Beau.  Joe mentioned my mother, who died of cancer.  She was two and a half years younger than I am today when she passed away.  


	And so it’s not always easy to remember, but being able to honor those we’ve lost in this way and to know that we may be able to prevent other families from feeling that same loss, that makes it a good day.  And I’m confident that it will lead to better years and better lives for millions of Americans, the work that you’ve done.  That’s what we got sent here for.  And it’s not always what we do.  It’s a good day to see us doing our jobs.


	So with that, I think it’s time for me to sign this bill into law.  (Applause.)


	(The bill is signed.)  


	END 
	3:16 P.M. EST    

