
	WASHINGTON, DC — In this week’s address, President Obama discussed the importance of ensuring that American families have access to paid leave. Today, millions of Americans do not have access to paid sick leave and have to face tough choices when their families face illness – choices that could risk their jobs or their health. The President has repeatedly called on Republicans in Congress to pass a law guaranteeing most workers the chance to earn seven days of paid sick leave each year. To this day, no action has been taken in the Republican Congress. But, that hasn’t stopped the President from taking action where he can to help more Americans access paid sick time, and starting January 1st, federal contractors will be required to give their employees working on new federal contracts up to seven paid sick days each year. That’s because paid leave isn’t something that just nice to have – it’s a must have.


	 


	 

